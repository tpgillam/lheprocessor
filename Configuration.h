#ifndef LHEPROCESS_CONFIGURATION_H
#define LHEPROCESS_CONFIGURATION_H

class Configuration {
 public:
  static Configuration &getInstance() {
    static Configuration instance;
    return instance;
  }

 public:
  bool fourVecEXYZ;
  bool noWeight;

 private:
  Configuration() : fourVecEXYZ(false), noWeight(false) {}
};

#endif  // LHEPROCESS_CONFIGURATION_H
