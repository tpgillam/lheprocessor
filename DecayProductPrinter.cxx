#include "Configuration.h"
#include "DecayProductPrinter.h"

void DecayProductPrinter::printAndReset(LorentzVectors_t::iterator &start) {
  LorentzVectors_t::iterator toReset(start);
  print(start);
  const LorentzVectors_t::const_iterator afterPrint(start);
  for (; toReset != afterPrint; ++toReset) {
    toReset->set(0.0, 0.0, 0.0, 0.0);
  }
}

void ComponentPrinter::print(
    LorentzVectors_t::iterator &start) {
  const auto &decayProduct = *start; ++start;
  const Configuration &configuration = Configuration::getInstance();
  if (configuration.fourVecEXYZ) {
    std::cout << " " << decayProduct.e() << " " << decayProduct.px() << " "
              << decayProduct.py() << " " << decayProduct.pz();
  } else {
    std::cout << " " << decayProduct.perp() << " " << decayProduct.eta() << " "
              << decayProduct.phi() << " " << decayProduct.m();
  }
}

void MassPrinter::print(
    LorentzVectors_t::iterator &start) {
  const auto &decayProduct = *start; ++start;
  std::cout << " " << decayProduct.m();
}

void RestFrameCosAnglePrinter::print(
    LorentzVectors_t::iterator &start) {
  const auto &vec1 = *start; ++start;
  const auto &vec2Ref = *start; ++start;
  auto vec2(vec2Ref);

  vec2.boost(-vec1.boostVector());
  double cosAngle = vec1.v().unit().dot(vec2.v().unit());
  std::cout << " " << cosAngle;
}
