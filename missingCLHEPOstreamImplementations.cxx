#include <CLHEP/Vector/LorentzVector.h>
#include <iostream>

namespace CLHEP {
std::ostream &operator<<(std::ostream &os, const HepLorentzVector &v) {
  return os << "(" << v.x() << "," << v.y() << "," << v.z() << ";" << v.t()
            << ")";
}

std::ostream &operator<<(std::ostream &os, const Hep3Vector &v) {
  return os << "(" << v.x() << "," << v.y() << "," << v.z() << ")";
}
}

