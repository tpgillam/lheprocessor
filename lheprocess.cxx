#include <CLHEP/Vector/LorentzVector.h>

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stack>
#include <algorithm>

#include <boost/algorithm/string.hpp>

#include "prettyprint.hpp"

#include "Configuration.h"
#include "DecayProductPrinter.h"

typedef int PDGCode_t;
typedef std::vector<PDGCode_t> PDGGroup_t;
typedef std::vector<PDGGroup_t> PDGGroupSet_t;

void generatePDGMappingsAndPrinters(
    const std::string &format, PDGGroupSet_t &pdgMappings,
    std::vector<DecayProductPrinter *> &printers) {
  std::stack<int> bracketTracker;
  std::ostringstream groupStream;
  std::ostringstream formatTokenStream;
  std::vector<std::string> tokens;
  int tempValue;
  int groupsToAbsorb(0);
  for (unsigned int i = 0; i < format.size(); ++i) {
    if (format[i] == '(') {
      bracketTracker.push(1);

      if (groupsToAbsorb == 0) {
        if (formatTokenStream.str() == "mass") {
          printers.push_back(new MassPrinter());
        } else if (formatTokenStream.str() == "restFrameCosAngle") {
          printers.push_back(new RestFrameCosAnglePrinter());
          ++groupsToAbsorb;
        } else {
          printers.push_back(new ComponentPrinter());
        }
        ++groupsToAbsorb;
      } 

      pdgMappings.push_back(PDGGroup_t());
      --groupsToAbsorb;

      groupStream.str("");
      groupStream.clear();
      // TODO
      formatTokenStream.str("");
      formatTokenStream.clear();
    } else if (format[i] == ')') {
      if (bracketTracker.empty() || bracketTracker.top() != 1) {
        throw std::runtime_error(
            "Invalid format string -- brackets don't match!");
      }
      bracketTracker.pop();
      std::string groupString(groupStream.str());
      boost::split(tokens, groupString, boost::is_any_of("\t "),
                   boost::token_compress_on);
      for (const auto &tok : tokens) {
        std::istringstream(tok) >> tempValue;
        pdgMappings.back().push_back(tempValue);
      }
    } else if (!bracketTracker.empty() && bracketTracker.top() == 1) {
      groupStream << format[i];
    } else if (format[i] != ' ') {
      // Some kind of printing token
      formatTokenStream << format[i];
    }
  }
}

void printUsage() {
  std::cout << "To print out tau 4-mom:\n"
            << "\t./lheprocess '(15)'" << std::endl;
  std::cout << "To print out two 4-mom, with the second from a positron and "
               "electron neutrino:\n"
            << "\t./lheprocess '(15) (-11 12)'" << std::endl;
  std::cout << std::endl;
  std::cout << "First column of output will always be the event weight."
            << std::endl;
  std::cout << "Can also include [OPTIONS] before format string:" << std::endl
            << "  e.g. fourVecEXYZ" << std::endl << "  e.g. noWeight"
            << std::endl << std::endl;
  std::cout << "The format string can include certain directives:\n"
            << "\t./lheprocess 'mass (25) restFrameCosAngle (25) (15)'"
            << std::endl;
  std::cout << "This will absorb the appropriate number of groups "
               "and print out one or more columns corresponding "
               "to the output" << std::endl;
  std::cout << std::endl;
}

int main(int argc, char *argv[]) {
  // Improve reading performance from stdin
  std::cin.sync_with_stdio(false);

  Configuration &configuration = Configuration::getInstance();

  if (argc < 2) {
    printUsage();
    std::cerr << "Must provide format string" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::string format;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "fourVecEXYZ") == 0) {
      configuration.fourVecEXYZ = true;
    } else if (strcmp(argv[i], "noWeight") == 0) {
      configuration.noWeight = true;
    } else {
      format += argv[i];
    }
  }
  std::vector<DecayProductPrinter *> printers;
  PDGGroupSet_t pdgMappings;
  generatePDGMappingsAndPrinters(format, pdgMappings, printers);

  long int eventNum(0);
  int partNum(0);

  bool startedEvents(false);
  bool onEventHeaderLine(false);
  std::string line;
  std::vector<std::string> tokens;

  PDGCode_t pdgCode;
  double eventWeight;
  double px, py, pz, E;
  CLHEP::HepLorentzVector tempVec;
  LorentzVectors_t groupedDecayProducts(pdgMappings.size());

  while (std::getline(std::cin, line)) {
    boost::trim(line);
    if (line == "") continue;
    if (boost::starts_with(line, "#")) continue;
    if (!startedEvents && boost::contains(line, "<event>")) {
      startedEvents = true;
    }

    if (!startedEvents) continue;

    if (onEventHeaderLine) {
      onEventHeaderLine = false;
      boost::split(tokens, line, boost::is_any_of("\t "),
                   boost::token_compress_on);
      std::istringstream(tokens[2]) >> eventWeight;
      continue;
    }

    if (boost::contains(line, "<event>")) {
      // Initialse at beginning of event
      onEventHeaderLine = true;
      partNum = 0;
      ++eventNum;
      continue;
    } else if (boost::contains(line, "</event>")) {
      // Finalise at end of each event
      if (!configuration.noWeight) {
        std::cout << eventWeight;
      }

      LorentzVectors_t::iterator decayProductIt = groupedDecayProducts.begin();
      for (const auto &printer : printers) {
        printer->printAndReset(decayProductIt);
      }
      std::cout << std::endl;
      continue;
    }

    ++partNum;

    boost::split(tokens, line, boost::is_any_of("\t "),
                 boost::token_compress_on);
    std::istringstream(tokens[0]) >> pdgCode;

    for (int i = 0; i < pdgMappings.size(); ++i) {
      const PDGGroup_t &thisGroup = pdgMappings[i];
      if (std::find(thisGroup.begin(), thisGroup.end(), pdgCode) !=
          thisGroup.end()) {
        std::istringstream(tokens[6]) >> px;
        std::istringstream(tokens[7]) >> py;
        std::istringstream(tokens[8]) >> pz;
        std::istringstream(tokens[9]) >> E;
        tempVec.set(px, py, pz, E);
        groupedDecayProducts[i] += tempVec;
      }
    }
  }

  // Clean up
  for (auto &printer : printers) {
    delete printer;
  }

  return EXIT_SUCCESS;
}
