PROG := lheprocess
SRCS := lheprocess.cxx DecayProductPrinter.cxx missingCLHEPOstreamImplementations.cxx
OBJS := $(subst .cxx,.o,$(SRCS))
CPP  := g++
CFLAGS := -std=c++11 $(shell pkg-config --cflags clhep)
LDFLAGS := $(shell pkg-config --libs clhep)

.PHONY: clean run

default: $(PROG)

$(PROG): $(OBJS)
	$(CPP) -o $@ $(OBJS) $(LDFLAGS)

%.o: %.cxx
	$(CPP) -o $@ -c $< $(CFLAGS)

test: $(PROG)
	# ./lheprocess
	echo "Default"
	zcat test.lhe.gz | head -n 792 | ./$(PROG) "mass (25) restFrameCosAngle (25) (15) restFrameCosAngle (25) (-11 12 -16)"
	echo "fourVecEXYZ"
	zcat test.lhe.gz | head -n 792 | ./$(PROG) fourVecEXYZ "mass (25) restFrameCosAngle (-11 12 -16) (-16) (-11 12 -16) (15 -11 12 -16)"

clean:
	@rm -f $(PROG)
	@rm -f *.o
