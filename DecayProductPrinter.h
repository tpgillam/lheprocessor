#ifndef LHEPROCESS_DECAY_PRODUCT_PRINTER_H
#define LHEPROCESS_DECAY_PRODUCT_PRINTER_H

#include <CLHEP/Vector/LorentzVector.h>
#include <vector>

typedef std::vector<CLHEP::HepLorentzVector> LorentzVectors_t;

class DecayProductPrinter {
 public:
  void printAndReset(LorentzVectors_t::iterator &start);
  virtual void print(LorentzVectors_t::iterator &start) = 0;
};

class ComponentPrinter : public DecayProductPrinter {
 public:
  virtual void print(LorentzVectors_t::iterator &start);
};

class MassPrinter : public DecayProductPrinter {
 public:
  virtual void print(LorentzVectors_t::iterator &start);
};

class RestFrameCosAnglePrinter : public DecayProductPrinter {
 public:
  virtual void print(LorentzVectors_t::iterator &start);
};

#endif  // LHEPROCESS_DECAY_PRODUCT_PRINTER_H
